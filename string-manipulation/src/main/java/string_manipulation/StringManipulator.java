package string_manipulation;

public class StringManipulator {

    public String reverse(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = s.length() - 1; i >= 0; i--) {
            builder.append(s.charAt(i));
        }
        return builder.toString();
    }

    public String dotToScreamingSnake(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '.' || s.charAt(i) == '-') {
                builder.append("_");
            } else {
                builder.append(s.charAt(i));
            }
        }
        return builder.toString().toUpperCase();
    }
}
