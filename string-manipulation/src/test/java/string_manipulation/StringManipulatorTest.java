package string_manipulation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StringManipulatorTest {

    StringManipulator manipulator = new StringManipulator();

    @Test
    void reverse() {
        String s = "cinnamon";
        String actual = manipulator.reverse(s);
        assertEquals("nomannic", actual);
    }

    @Test
    void dotToScreamingSnake() {
        String s = "application.name";
        String actual = manipulator.dotToScreamingSnake(s);
        assertEquals("APPLICATION_NAME", actual);
    }
}
