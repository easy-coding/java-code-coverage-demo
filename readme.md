# Java code coverage demo

This demo shows how to use JaCoCo with Maven to display the code coverage.

## Install Jacoco on a single module

To view the code coverage of a single module, use the `jacoco-maven-plugin`.

    <!-- https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin -->
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.5</version>
        <executions>
            <execution>
                <id>pre-unit-test</id>
                <goals>
                    <goal>prepare-agent</goal>
                </goals>
            </execution>
            <execution>
                <id>post-unit-test</id>
                <phase>test</phase>
                <goals>
                    <goal>report</goal>
                </goals>
            </execution>
        </executions>
    </plugin>

The plugin will prepare a java agent before the tests execute to be able to do its work.
When unit tests complete the agent creates the report in `target/site/jacoco`.

## Aggregate all coverage

To be able to get all the coverage data at once, create an additional module.
This module must depend on all the modules you want to test.
This way it will execute last when coverage data is available for every other module.

    <!-- https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin -->
    <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.8.5</version>
        <executions>
            <execution>
                <id>post-unit-test</id>
                <phase>test</phase>
                <goals>
                    <goal>report-aggregate</goal>
                </goals>
            </execution>
        </executions>
    </plugin>

The plugin will aggregate all coverage data and put it in `target/site/jacoco-aggregate`.

## Display it on gitlab

In the `gitlab-ci.yml` after tests you have to output the code coverage in the console.

    - COVERAGE=`cat reporting/target/site/jacoco-aggregate/index.html | grep -oP '(?<=class="ctr2">)[0-9]*(?=%)'  | head -1 | sed 's/,//g'`
    - echo COVERAGE $COVERAGE%

This will produce the following output:

    $ COVERAGE=`cat reporting/target/site/jacoco-aggregate/index.html | grep -oP '(?<=class="ctr2">)[0-9]*(?=%)'  | head -1 | sed 's/,//g'`
    $ echo COVERAGE $COVERAGE%
    COVERAGE 100%
    Job succeeded

Gitlab must also know that it must look for this particular expression. There are two ways to do it:
- on the job use the following property `coverage: '/COVERAGE ([0-9]+)%/'`
- or set it in the project setting: `Settings > CI / CD Settings > General pipelines > Test coverage parsing`

Then display it proudly on your readme like this:

[![pipeline status](https://gitlab.com/easy-coding/java-code-coverage-demo/badges/master/pipeline.svg)](https://gitlab.com/easy-coding/java-code-coverage-demo/-/commits/master)
[![coverage report](https://gitlab.com/easy-coding/java-code-coverage-demo/badges/master/coverage.svg)](https://gitlab.com/easy-coding/java-code-coverage-demo/-/commits/master)

The markdown code for your project can be found just after the test coverage parsing in the project settings.

## What not to cover

Some files are pointless to test and should be excluded from JaCoCo. These files include:
- generated code
- configuration
- main method

### Generated code

In version 0.8.2 or better, JaCoCo will not cover code that has a `@Generated` annotation.
This works fine as long as the annotation has a `RUNTIME` retention policy.
If the retention policy is `SOURCE` the compiler will discard it for tests.

#### Lombok

Lombok will include the generated annotation if you include the `lombok.config` at the root of your project with
the following:

    lombok.addLombokGeneratedAnnotation = true

#### Mapstruct

Unfortunately Mapstruct generates implementations with `@Generated` annotation that has `SOURCE` retention policy.
My personal workaround for it is to postfix all my mappers' name with `Mapper` and exclude all `.*MapperImpl` files.

    <configuration>
        <excludes>
            <exclude>**/*MapperImpl.*</exclude>
        </excludes>
    </configuration>

### Configuration

Configuration files like spring's `@Configuration` should be excluded.
Pick a suffix like `Config` or `Configuration` and exclude it.

    <configuration>
        <excludes>
            <exclude>**/*Configuration.*</exclude>
        </excludes>
    </configuration>

### Main method

You should not be required to launch the entire app to test it.
In the same fashion, pick a name for your main class and exclude it.
