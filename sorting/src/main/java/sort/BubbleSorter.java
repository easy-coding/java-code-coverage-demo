package sort;

import java.util.ArrayList;
import java.util.List;

public class BubbleSorter {

    public <T extends Comparable<T>> List<T> sort(List<T> list) {
        List<T> copy = new ArrayList<T>(list);
        for (int i = 0; i < copy.size(); i++) {
            for (int j = i; j < copy.size(); j++) {
                if (copy.get(i).compareTo(copy.get(j)) > 0) {
                    T swap = copy.get(j);
                    copy.set(j, copy.get(i));
                    copy.set(i, swap);
                }
            }
        }
        return copy;
    }
}
