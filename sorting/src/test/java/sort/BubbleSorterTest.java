package sort;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BubbleSorterTest {

    BubbleSorter sorter = new BubbleSorter();

    @Test
    void sort() {
        List<String> words = List.of("cat", "zebra", "dog");
        List<String> actual = sorter.sort(words);
        assertEquals(List.of("cat", "dog", "zebra"), actual);
    }
}
